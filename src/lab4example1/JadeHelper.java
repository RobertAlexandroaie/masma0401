package lab4example1;

import jade.core.Runtime;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class JadeHelper 
{
    public static jade.wrapper.AgentContainer CreateContainer(String containerName, boolean isMainContainer, String hostAddress, String hostPort, String localPort) 
    {
        ProfileImpl p = new ProfileImpl();

        if (!containerName.isEmpty()) 
        {
            p.setParameter(Profile.CONTAINER_NAME, containerName);
        }

        p.setParameter(Profile.MAIN, String.valueOf(isMainContainer));

        if (localPort != null) 
        {
            p.setParameter(Profile.LOCAL_PORT, localPort);
        }

        if (!hostAddress.isEmpty()) 
        {
            p.setParameter(Profile.MAIN_HOST, hostAddress);
        }

        if (!hostPort.isEmpty()) 
        {
            p.setParameter(Profile.MAIN_PORT, hostPort);
        }

        if (isMainContainer == true) 
        {
            return Runtime.instance().createMainContainer(p);
        } else 
        {
            return Runtime.instance().createAgentContainer(p);
        }
    }

    public static jade.wrapper.AgentController CreateAgent(jade.wrapper.AgentContainer container, String agentName, String agentClass, Object[] args) 
    {
        AgentController ag = null;
        try
        {
            ag = container.createNewAgent(agentName, agentClass, args);
        }catch(StaleProxyException e){}
        
        return ag;
    }
}
